gradle assemble &&
MAINCLASS=$(cat build/bootJarMainClassName) &&
JAR=$(ls build/libs/) &&
mkdir build/libs/native-image &&
cd build/libs/native-image &&
jar -xvf ../example-native-service-0.0.1-SNAPSHOT.jar >/dev/null 2>&1 &&
cp -R META-INF BOOT-INF/classes &&
LIBPATH=`find BOOT-INF/lib | tr '\n' ':'` &&
CP=BOOT-INF/classes:$LIBPATH &&
time native-image --no-server --no-fallback -H:Name=example-app -H:+ReportExceptionStackTraces -Dspring.graal.remove-unused-autoconfig=true -Dspring.graal.remove-yaml-support=true -cp $CP -H:Class=de.markant.mnet.example.examplenativeservice.ExampleNativeServiceApplication;
